package com.vynex.upread;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUtils extends AsyncTask<String, Integer, String> {
    public final String apiLink = "https://anycat.rocks/unacademy";

    @Override
    protected String doInBackground(String ...params) {
        try {
            URL url = new URL(apiLink);
            String data = params[1];

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            try {
                connection.setDoOutput(true);
                connection.setChunkedStreamingMode(0);

                OutputStream outputStream = new BufferedOutputStream(connection.getOutputStream());
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);

                outputStreamWriter.write(data);
                outputStreamWriter.flush();
                outputStreamWriter.close();

                Log.d("Response Code", "" + connection.getResponseCode());
                Log.d("Response Message", connection.getResponseMessage());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                connection.disconnect();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
