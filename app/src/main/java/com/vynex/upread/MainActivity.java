package com.vynex.upread;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.service.notification.NotificationListenerService;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
        startActivity(intent);
        startService(new Intent(this, NotificationListener.class));
    }
}