package com.vynex.upread;

import android.app.Notification;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import org.json.JSONObject;

public class NotificationListener extends NotificationListenerService {
    @Override
    public void onCreate(){
        Log.d("NotifListener", "Logged");
        super.onCreate();
    }

    @Override
    public void onListenerConnected(){
        super.onListenerConnected();
        Log.d("onListenerConnected","Connected!");
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn){
        super.onNotificationPosted(sbn);

        Bundle extras = sbn.getNotification().extras;
        interceptNotification(extras.get(Notification.EXTRA_TEXT));
    }

    public void interceptNotification(Object notification) {
        if (notification == null) return;

        if (notification.toString().contains("Unacademy")) {
            HttpUtils httpWorker = new HttpUtils();

            Log.d("Notification Received", notification.toString());

            try {
                JSONObject requestData = new JSONObject();
                requestData.put("msgText", notification.toString());

                httpWorker.execute("", requestData.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
